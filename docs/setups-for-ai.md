# Setups for AI

## Why create setups for AI?

TO BE COMPLETED



## Where to put AI setups? How to name them?

### rFactor 1

In [this RaceDepartment post](https://www.racedepartment.com/threads/ai-editing-the-strength-of-a-mod-need-advice.228552/post-3520392), jgf explains:

> Give the AI setups for each car and track; first set `Fixedsetups=1` and `FixedAIsetups=1` in your PLR file. I use my own setups but with top gear extended a click or two (otherwise the AI are likely to blow the engine). For example, you create a setup for your Mustang at Goodwood, set the top gear a little higher and save with different name; copy that setup (svm file) to the root folder of that Mustang (where the hdv and pm files are located) and rename it `Goodwood.svm`, now any AI driving that car at that track will use that setup. Look in the track's GBR file for `AISetting` for the proper name for the setup, otherwise it will be ignored.

### SimBin sims: GTR 2, RACE 07

TO BE COMPLETED

### Automobilista 1

TO BE COMPLETED


