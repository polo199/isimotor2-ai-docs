# Car files




## Damage files

### "AI slowdown" bug, fix

Many players of isiMotor games report that with some mods or default content in long races, after a long period of time, the pace of many (if not all) AI cars drops significantly. This can ruin long single player races! This bug is commonly referred to as the **"AI slowdown"** bug.

The [Von Dutch AI Tutorial](https://www.racedepartment.com/downloads/von-dutch-ai-mod-and-tutorial.23792/) explains:

> The "AI slowdown" problem is actually an aero damage issue. The AI bump and grind with each other (especially if you have raised the aggression level) and damage each other to the point where they slow down significantly over the course of a race.
> 
> - First you need to go to the **.dmg (damage) file of each and every team that you want to race against.** 
> - Look for this line: **"AeroMin=1100.0"**
> - Raise this to **7500.0 or higher.**
> 
> What this does is raise the minimum amount of force that it takes for the AI (or your car for that matter) to become damaged. This number takes some trial and error to get right, but 7500 is a good start. Remember that the number can be different for the different vehicles in the game/race.





## HDV, HDC file tweaks

For rFactor 1 and Automobilista 1, the main car files to edit are called `HDV` files. For SimBin titles like GTR2 and RACE 07, these are `HDC` files.

### AITorqueStab

The following code comment to describe the meaning of `AITorqueStab` can be found in isiMotor files:

```
// Torque adjustment to keep AI stable
```

Default rFactor values are `AITorqueStab=(1.00,1.00,1.00)`.

The numbers represent how much force is needed to deflect the car from its course, longitudinally, laterally, and vertically.[^1]

jgf recommends:

> Run some practice sessions with the AI in the same cars as you and do a little "paint swapping"; if your car reacts more than the AI, lower the values, if the AI react more than you, raise the values. Set this too low and the slightest nudge will send the AI out of control.

Modders have long recommended lowering the `AITorqueStab` values in order to prevent AI cars from being unrealistically stable and near-impossible to 'spin out' (even with heavy contact). Lower values can help it seem that AI cars are subject to similar contact physics to the player, though there is also the risk of making AI cars too unstable and too easy to spin out.

Note that this can be achieved by directly changing the `HDV` or `HDC` files, and *can also* be achieved in rFactor 1 and Automobilista 1 by changing the vehicle's `upgrades.ini` file – as explained [in this post on the Reiza forums](https://forum.reizastudios.com/threads/howto-reduce-excessive-ai-stability-sp-only.2078/).

Renato Simioni from Reiza Studios wrote the following in 2017 about AMS 1 at [this link](https://forum.reizastudios.com/threads/howto-reduce-excessive-ai-stability-sp-only.2078/):

> FYI, we're using 1.0, 1.0, 1.5 values for most cars now. The third value is a roll stabilizer which helps keeping them stable when hitting high curbs - as far as yaw / pitch axis though they should be the same as the player. However AI physics also involve different tire grip multipliers, aero bias, weight distribution and CoG height which are required both for stability & performance balance, which may still give them some extra stickiness when in contact to the player.

### AIPerfUsage

The following code comment to describe the meaning of `AIPerfUsage` can be found in isiMotor files:

```
// PLR file overrides for (brake power usage, brake grip usage, corner grip usage) used by AI to estimate performance - only positive values will override, see PLR for defaults
```

rFactor 1 default values: `AIPerfUsage=(1.00, 1.00, 1.00)`

### Suspension tweaks

The [Von Dutch AI Tutorial](https://www.racedepartment.com/downloads/von-dutch-ai-mod-and-tutorial.23792/) (which is intended for GTR 2 default content) suggests:

> Find these lines in your **.hdc**;
> 
> - **AIEvenSuspension=0.0** // averages out spring and damper rates to improve stability (0.0 - 1.0) 
> - **AISpringRate=1.0** // spring rate adjustment for AI physics (improves stability) 
> - **AIDamperSlow=1.2** // contribution of average slow damper into simple AI damper 
> - **AIDamperFast=0.4**
> 
> DO NOT make big changes here or the AI won’t be able to complete a lap!!! 
What's working for me now is this:
> 
> - **AIEvenSuspension=0.1** // averages out spring and damper rates to improve stability (0.0 - 1.0) 
> - **AISpringRate=1.0** // spring rate adjustment for AI physics (improves stability) 
> - **AIDamperSlow=1.1** // contribution of average slow damper into simple AI damper 
> - **AIDamperFast=0.3**

Here is what each of these parameters mean:[^2]

> `AIEvenSuspension`: Averages out spring and damper rates to improve stability (0.0 - 1.0). The use of this variable would increase the disparity between AI and player physics.
>
> `AISpringRate`: Spring rate adjustment for AI physics is used to soften or stiffen the suspension for AI vehicles. Decrease this to soften spring rates to keep the AI stable. 1 is equivalent to the setup file or default setup.
>
> `AIDamperSlow`: Contribution of average slow damper into simple AI damper. This simplifies dampers when used for AI.
>
> `AIDamperFast`: Contribution of average fast damper into simple AI damper.

### Collision detection adjustments

- `AIMinPassesPerTick=0`: The default values are too low (e.g. 2) so you must increase to 6, 7 or even 10. If you want the AI cars being simulated in real time, set it to zero, but anything above 6 will produce a more realistic AI physics simulation. However, this will demand more CPU, so if you don't bare a good i5 or i7 is better to be careful and avoid 10 or zero values.
- `FeelerFlags=15`: This will create the most possible accurate collision detection system, but you'll need to use the collision feelers. This part would be optional, since you'll need 3DSimEd to get the real car dimensions for each competitor. But, if you willing too, it's a worth effort.

Procedure to follow to improve collision detection of AI cars:

1. Delete the `//` (comment marker) from the collision feelers lines and remove the line `FeelerFlags=X` from the top of it, since it's already informed at the beginning of `HDC` file.
2. You'll notice there's a bug in most of cars I've seen so far which is the wrong signal for `FeelerTopFrontLeft`, `FeelerTopFrontRight`, `FeelerTopRearLeft` and `FeelerTopRearRight`. 
    - Note that the coordinate system is strange (at least for me): +x = left, +y = up, +z = rear. The lines below are already fixed, so pay attention on it. Left is always positive and right is always minus!
3. To know the real car dimensions, you'll have to open it in 3DSimEd. Take note of dimensions, subtract 10 cm from each one (helps to prevent the AI to AI crash without significant visual glitches), change the values above (double checking the signals + and -)
    - Remember to leave the rear-wing out of the box!
    - It's not nice to see the car turning over based in the rear-wing position, it's preferable to leave the wing going through the tarmac than the strange "rigid rear-wing" effect when turning over

If you have patience enough, the amount of AI to AI crashes and the physics reaction will improve a lot by following this procedure. If you don't use feelers, the game will create a automatic box around the car which could be pretty rough for calculations.[^3]

The section of the `HDC` file relevant to collision detection might look something like this:

```json
//FeelerFlags=15 // or remove this second instance or simply leave the "//" infront
FeelerOffset=(0.0, 0.0, 0.0) // leave it alone, it won't be used.
FeelersAtCGHeight=0 // Set it to zero. You'll provide all real corners coordinates for the car.
FeelerFrontLeft=(1.003,0.384,-2.554) //front-left corner collision feeler
FeelerFrontRight=(-1.003,0.384,-2.554) // front-right corner collision feeler
FeelerRearLeft=(1.003,0.384,2.586) //rear-left corner collision feeler
FeelerRearRight=(-1.003,0.384,2.586) // rear-right corner collision feeler
FeelerFront=(0.064,0.384,-2.639) // front side collision feeler
FeelerRear=(0.064,0.384,2.587) // rear side collision feeler
FeelerRight=(-1.039,0.384,-0.247) // right side collision feeler
FeelerLeft=(1.039,0.384,-0.247) // left side collision feeler
FeelerTopFrontLeft=(0.478,1.540,-0.298) // top front-left collision feeler
FeelerTopFrontRight=(-0.478,1.540,-0.298) // top front-right collision feeler
FeelerTopRearLeft=(0.652,1.511,2.323) // top rear-left collision feeler
FeelerTopRearRight=(-0.652,1.511,2.323) // top rear-right collision feeler
FeelerBottom=(0.064,0.249,-0.247) // bottom feeler
```


### Brakes and grip adjustments

- `RearBrakeSetting=36` 
      - Some modders make a little confusion here and put improper values to this variable, causing oversteering in AI cars and causing havoc on some tracks
      - Example: Daytona Road Course, where the first corner is after a really hard braking zone where the wheels are still steering
          - Using the value = 36 (or so) you'll have a AI brake distribution (64:36) which can be very bad for human drivers, but it's good for AI
          - I often see values as 60 or 55 here, and it means a brake distribution (40:60) or (45:55) which is way wrong and induces demolition derby due to a lot of spinning
          - Remember when you adjust this value the default setup car will be 64:36 (in this case) so, you should change it before drive your car (or on the fly)
- `BrakePressureRange=(0.00, 0.01, 101)` and `BrakePressureSetting=100`
    - I caught these values from AI Reborn and it seems to help a lot on preventing AI to AI crashes
    - Theoretically it shouldn't be like this since the default value is 80 + 20 = 100 % of pressuring but for some reason it doesn't
- `ABS4Wheel=1` and `ABSGrip=(1.05, 0.25)`
    - Those lines give more brake effectiveness for AI
    - Default are 1.00, 0.20 but I found with those values there's less AI rear ending during the races
- `AIDownforceBias=1.0`
    - This is a percentage between the AI setup downforce (0.0) and scripted downforce calculated by the game (1.0)
    - If you're not using AI specific Setups, you should set the value to 1.0 and let the computer calculates the correct downforce
    - BUT... in certain sort of tracks (e.g. Le Mans) with very long straights, to have a good AI Setup might be the better way to go (because it will not give proper gear ratios to AI, whereas a good setup does)
- `AIMinRadius` 
    - This is tricky, from what I can tell it controls the distance at which they are aware of other vehicles
    - Set too low and the AI is like NASCAR – constantly "trading paint"
    - Set too high and they are too busy staying out of each others' way to race effectively[^4]

There is also `AIDownforceZArm`, a hard-coded center-of-pressure offset from vehicle's centre-of-gravity.[^5]

### Fuel settings

`FuelSetting` sets the size of the car's fuel tank in litres.



[^1]: From jgf's RaceDepartment post [here](https://www.racedepartment.com/threads/did-you-know-theres-hidden-ai-settings-in-your-plr-profile.190990/#post-3514486).
[^2]: From [this RaceDepartment post](https://www.racedepartment.com/threads/ai-settings-just-a-bit-more-boring-ai-info.218549/post-3486714) by Speednut357.
[^3]: This entire section is copied and cleaned up (typos, grammar) from an old forum thread at NoGripRacing. It was shared [in this RaceDepartment post](https://www.racedepartment.com/threads/ai-and-hitting-stationary-cars.242913/post-3570580) by jgf. It was written with SimBin sims in mind, but should also be applicable to rF1 and AMS 1.
[^4]: This entire section is copied and cleaned up (typos, grammar) from an old forum thread at NoGripRacing. It was shared [in this RaceDepartment post](https://www.racedepartment.com/threads/ai-and-hitting-stationary-cars.242913/post-3570580) by jgf. It was written with SimBin sims in mind, but should also be applicable to rF1 and AMS 1.
[^5]: From [this RaceDepartment post by Speednut357](https://www.racedepartment.com/threads/ai-settings-just-a-bit-more-boring-ai-info.218549/post-3486714).

