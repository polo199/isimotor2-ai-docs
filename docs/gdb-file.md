# GDB file

## Track information

These parameters allow you to control the display name for the track, as well as other info about the track displayed in the UI.

Below is the entry for Caruaru, a track that ships with AMS 1 with unencrypted files:

```
  TrackName = Caruaru
  EventName = Caruaru
  GrandPrixName = Caruaru	//this must be the same as event name in order to sort circuit info correctly.
  VenueName = Caruaru
  Location = Caruaru, PE, Brazil
  Length = 2.82 km / 1.75 miles
  TrackType = National Circuit
  Track Record =
```

## Pit speed

`RacePitKPH` controls the pit speed limit in km/h during the race, while `NormalPitKPH` controls the pit speed limit in km/h during all other sessions.

## Formation lap

`FormationSpeedKPH` sets the speed of the formation lap in km/h.

## Grip of surfaces

`RoadGrip` variables defined in GDB are then used in the TDF file to define the grip of various surfaces on a given track.

TO BE COMPLETED

## Qualifying

Number of quali laps: only 8 or 12 may have AI going one extra lap and getting disqualified!

TO BE COMPLETED

## GarageDepth

`GarageDepth`

TO BE COMPLETED

## Automobilista 1: tyre compounds

`CompoundsAllowed` controls which tyre compounds can be used in Qualifying sessions and Race sessions on a per-series basis.

Below is the entry for Caruaru, a track that ships with AMS 1 with unencrypted files:

```json
  CompoundsAllowed
 {
    Series8  
    {
        Quali=2,1,3,4
        Race=2,1,3,4
    }
    Series9  
    {
        Quali=2,1,3,4
        Race=2,1,3,4
    }
	Series12  
    {
        Quali=2,1
        Race=1,2
    }
	Series15  
    {
        Quali=1,2
        Race=1,2
    }
	Series18  
    {
        Quali=1,2
        Race=1,2
    }
    Series31  
    {
        Quali=2,3,1,4,5
        Race=2,3,1,4,5
    }
 }
}
```
