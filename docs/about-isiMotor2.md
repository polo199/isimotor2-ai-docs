# About isiMotor2, the game engine



## isiMotor1

The prehistory of isiMotor2 begins with a string of games built on the isiMotor1 game engine developed by Image Space Incorporated (ISI) in the late 1990s and early 2000s: *Sports Car GT* (released 1999), *Hot Rod Monster Squad*, *F1 2000*, *F1 Championship Season 2000*, *F1 2001*, *F1 2002*, *F1 Challenge '99-'02*, *NASCAR Thunder 2003* and *NASCAR Thunder 2004*. All titles except *Hot Rod Monster Squad* were published by Electronic Arts (EA), and the F1 and NASCAR titles were officially licensed. Many of these titles became popular. One key reason for this: isiMotor1 proved to be highly moddable, and users created many new paint schemes, cars, and tracks for isiMotor1 titles, often porting popular mods between games.[^1]



## isiMotor2

After years of independent development by ISI, a new game engine, isiMotor2, was ready for use and was licensed to SimBin Studios for their upcoming title *FIA GT Racing Game: GTR*. SimBin had come into being by creating and releasing a free, unlicensed [FIA GT](https://en.wikipedia.org/wiki/FIA_GT_Championship) mod for *F1 Challenge* – for more info, see [this YouTube video by GPLaps](https://youtu.be/JEDyNgurBSM). They were now developing a licensed game of their own after purchasing a license for the right to use isiMotor2 from ISI. This was the first of many sim racing titles built on isiMotor2. Over time, a number of different developers would license isiMotor2 for use in their games. A list of dev studios and games is below.

- ISI
    - [rFactor](https://store.steampowered.com/app/339790/rFactor/) (now often referred to as rFactor 1, or rF1)
- SimBin Studios
    - [GTR](https://store.steampowered.com/app/44600/GTR__FIA_GT_Racing_Game/)
    - [GTR 2](https://store.steampowered.com/app/8790/GTR_2_FIA_GT_Racing_Game/) (along with Blimey! Games)
    - [RACE 07](https://store.steampowered.com/app/8600/RACE_07/) and its many expansion packs (GTR Evolution, STCC – The Game, Race On, Formula Raceroom, STCC – The Game 2, GT Power, WTCC 2010, RETRO, Race Injection)
- Blimey! Games
    - BMW M3 Challenge
- Reiza Studios
    - Game Stock Car (2012, 2013, and then renamed [Stock Car Extreme](https://store.steampowered.com/app/273840/Stock_Car_Extreme/))
    - [Formula Truck](https://store.steampowered.com/app/273750/Formula_Truck_2013/)
    - [Copa Petrobras de Marcas](https://store.steampowered.com/app/359800/Copa_Petrobras_de_Marcas/)
    - [Automobilista](https://store.steampowered.com/app/431600/automobilista) (now often referred to as Automobilista 1 or AMS 1)
- Ignite Game Technologies
    - SimRaceway
- 2PEZ Games
    - Turismo Carretera
    - TopRace 2009
    - Carretera 2012
    - TC 2000
- EA
    - NASCAR SimRacing
- The Sim Factory
    - ARCA Sim Racing '08
    - ARCA Sim Racing X

Many of these developers used slightly different versions of isiMotor2, as many added their own in-house features to ISI's original game engine:

- SimBin's version of isiMotor2 used in GTR 2 and RACE 07 added features (e.g. rain, dynamic track surface, visual pitcrew) and uses file formats with different names (e.g. `.gtr`) that are somewhat differently structured than in rFactor 1
- rFactor 1 includes some improvements in isiMotor2 not included in SimBin's version
    - In particular, SimBin's isiMotor2 physics are optimized towards GT and touring cars
    - Whereas rFactor 1's physics are flexible, designed to be used with touring cars, GTs, open-wheelers, and more (and probably therefore slightly more sophisticated)
- As they released more games using isiMotor2, Reiza Studios began to fix bugs and add features in the engine, eventually licensing the full isiMotor2 source code from ISI and adding improved AI behaviour, higher physics and input refresh rates, dynamic track rubbering, visual marbles, turbo simulation, better gearbox simulation, dynamic dirt pickup, and more

isiMotor2, like isiMotor1 before it, is highly moddable. Some games have proved especially popular with modders, and even today (especially with high-quality mods installed), these games can offer a compelling, high-quality sim racing experience: **rFactor 1, GTR 2, RACE 07, and Automobilista 1 (AMS 1)**. This guide focuses on AI in these titles. We refer to GTR 2 and RACE 07 simultaneously as *SimBin titles*, since these two games are built on near-identical variants of isiMotor2. rFactor 1 and Automobilista 1 are also rather similar – in general, features that exist in rFactor 1 persist in Automobilista 1 (but not vice versa).



## Why is isiMotor often called gMotor?

While designing their in-house game engine, ISI named components according to their function i.e. gMotor for the graphics, pMotor for the physics. Hence, isiMotor2 is sometimes referred to interchangeably as gMotor2 (or gMotor for short) and games built on isiMotor2 will sometimes throw error messages referencing gMotor2 internal errors.[^1]



## What about rFactor 2?

According to ISI, rFactor 2 is built on isiMotor2.5. Compared to its predecessor, this game engine includes a sophisticated physical tyre model, numerous major physics parameter, track file format changes, and new AI features like a "blocking line". 

Although isiMotor2.5 is technically an evolution of isiMotor2 (and therefore shares some features and characteristics) there are too many differences from rF1, GTR2, RACE 07, or AMS 1 to include a comprehensive guide to rFactor 2 AI tweaking on this website. Even so, some features from isiMotor2 games (e.g. AIW file parameters, AIW fast path best practices) carry over to rF2. Therefore, it can be helpful to make reference to rF2, and we do so on occasion.



[^1]: [History of the Image Space Inc. Software Engine](https://web.archive.org/web/20150131043113/http://imagespaceinc.com/software.php).

