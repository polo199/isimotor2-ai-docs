# Thanks and acknowledgements

A massive thank you to everyone listed below!

- The RaceDepartment community, including:
    - MJQT (primary author, researcher, repo maintainer)
    - Shovas
    - jgf
    - Speednut357
    - AndreasFSC
    - Bjarne Hansen
    - GTR233
    - Bela Valko
    - Salvatore Sirignano
    - Maxell
    - Lorencini
- Bram Hengeveld and the rest of the RaceDepartment staff
- The NoGripRacing community
    - Von Dutch
    - Chronus (GTR 2 Reborn)
    - And many others...
- Austin Ogonoski
- Billy Strange
- The developers of these great driving and racing sims!
    - Image Space Incorporated
    - SimBin Studios
    - Reiza Studios
- The developers of the open-source tools used to build this site!
    - Python
    - MkDocs
    - Material for MkDocs


