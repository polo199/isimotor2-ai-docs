# AI improvement mods



## rF1 AI "learning"

### How to use it?

TO BE COMPLETED

### How does it help?

- Makes the AI adhere more closely to the "ideal" racing line as defined by the AIW file waypoints[^1]
    - So they will only be as good as that file allows (if you get an "improved" AIW file for the track you will have to train again)
    - They will only deviate from this line to pass, avoid collisions, or from the occasional driving error, so the width of the path only comes into play in those situations
- About the "yards left" messages[^2]
    - At the end of a training session you may get a notice like "80 yards left" – this means the line the AI drives is still 80 yards longer than the "ideal" line
    - Repeating the session should lower this; it may take several sessions but you can usually get that to zero or very close
    - If after several sessions you still have a notable discrepancy it is most likely the waypoints were designed for a quite different type of car and your car cannot follow that line exactly (a GT3 Viper and an F1 car would have quite different lines around the same track), but as long as it is using a reasonable line consistently the training is working (and "notable discrepancy" is relative, 50 yards at Mid-Ohio is a major problem, at Nurburgring it is virtually irrelevant)




## AI Improvement Plugin by JR

Download link from RaceDepartment: [click here](https://www.racedepartment.com/downloads/ai-improvement-plugin-by-jr.49622/)

TO BE COMPLETED



## SHO Competition AI

Download link from RaceDepartment: [click here](https://www.racedepartment.com/downloads/sho-competition-ai.30447/)

TO BE COMPLETED



## RACE 07 Smart AI

GitHub link: [click here](https://github.com/dbkblk/race07-smartai)

TO BE COMPLETED



[^1]: From jgf in [this RaceDepartment post](https://www.racedepartment.com/threads/ai-editing-the-strength-of-a-mod-need-advice.228552/post-3520853).
[^2]: From jgf in [this RaceDepartment post](https://www.racedepartment.com/threads/ai-editing-the-strength-of-a-mod-need-advice.228552/post-3520853).
