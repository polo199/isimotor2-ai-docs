# Talent (RCD) files



> **Note**: all AMS 1 suggested values were taken by Lorencini from the original SuperV8 talent files.



## Parameters common to all isiMotor2 sims[^1]

### Aggression

Basically controls how close that AI will get to other cars (AI or player); high value means they will go fender-to-fender with you, lower values makes them back off. This does not make AI directly faster, but lower values make them spend more time avoiding each other and you. Too high and they will sideswipe you, and each other, often causing damage; too low and they bob and weave all the time trying to stay apart.[^2]

Increases aggression also increases the frequency at which they try to pass, and increases the threshold they are willing to endure before giving up a pass (or crashing).[^3]

AMS1: Reiza talent files keep this value between 60 and 80

### Speed

Speed will define the AI lap times. This parameter is what makes an AI driver to be a top one, a mid-pack one, or a backmarker. Keep in mind that each level (from 98 to 99, for example) will give you a difference of 1 to 3 tenths of a second per lap... so, speed 65 is *really* slow. 

AMS1 suggested range: 65-99

*Quick tip*: in rF1 and AMS1, it appears that if you go to track and enabled computer-controlled car, your car becomes an AI car with speed=100 (it will be faster than any other AI in the suggested range).

### MinRacingSkill

When `AI Limiter` from the PLR file value is larger than 0.0 (which is true by default), the AI drivers go through cycles of optimal driving and sub-optimal driving where their driving skill falls to `MinRacingSkill` * `Speed`.[^3] In effect, `MinRacingSkill` determines how bad an AI driver's bad laps can be.

AMS 1 values: Reiza uses 60-99

### Composure 

The lower the number, the higher the frequency of "intentional" simulated driving mistakes from AI. Intentional" mistakes are things like, taking a turn too wide, or missing a braking point. Increasing `Composure` also decreases the time between bad driving zones.[^3]

Tweaking `Composure` can prevent AI cars from slowing down after you overtake them! isiMotor2 AI, by default, tend to drive faster (more confident) when ahead of you versus drive slower (less confident) when behind you (or, more correctly, once passed). This means overtaken AI cars tend to fall too far behind to battle with you, making for a less fun gameplay experience. [As observed by Shovas in his GTR 2 tweaking](https://www.racedepartment.com/downloads/sho-competition-ai.30447/updates#resource-update-86348), high values of **Composure** appear to prevent this bug!

SimBin values: range between 0.0 and 1.0

AMS1 suggested values: Reiza uses 66-98 range, if in doubt stick with 90

### Reputation 

Not used in AMS1: Reiza always uses value 99

### Crash and Recovery

These two work together. **Crash** is the probability of AI cars to crash. **Recovery** is the probability of avoidance in case of an imminent crash.

AMS 1: Reiza generally uses `Crash=1` and `Recovery=5` to almost all cars, with some troublemakers getting `Crash=10` and `Recovery=15`.

### CompletedLaps

Only used when AI autocomplete laps (e.g. skipping practice or qualifying).

AMS 1: Reiza uses a fixed value of 95

### Courtesy

This parameter appears in rF1 talent files, but its function is unclear.

In AMS 1, the higher the value, the quicker AI will respond to the blue flags in practice, qualifying and race sessions. Put higher values to the slow cars, so they can keep out of the leaders' way when they get lapped.

Usually varies from 60-95



## Exclusive to SimBin titles

### ColdBrain variables

There are four "ColdBrain" variables used in SimBin `RCD` files: `RaceColdBrainMin`, `RaceColdBrainTime`, `QualColdBrainMin`, and `QualColdBrainTime`.

This example nicely explains what these variables mean:

```ini
RaceColdBrainMin=0.93
RaceColdBrainTime=120
QualColdBrainMin=0.80
QualColdBrainTime=200
```

During the race, for the first 2 min (120 sec), the AI car is artificially slowed down to 93% of its 'normal' speed. Meanwhile, during qualifying, for the first 200 seconds, the AI car is artificially slowed down to 80% of its 'normal' speed.[^4]

### Example SimBin RCD File

Below is an example talent file for RACE 07, including comments to explain some of the parameters.[^5]

Note that the name of the `RCD` file (e.g. `Fred Flintstone.ini`) must match the in-game driver name for the car (e.g. "Fred Flintstone").

```ini
{
//Driver Info
Abbreviation=F.Flintstone
Nationality=American
NatAbbrev=USA
DateOfBirth=05.05.01
CareerHighlights
{
4th In Bedrock Rally
3rd In Bedrock GP 2007
}

//Driver Stats
StartsDry=2.9 //Average number of drivers passed during start (-4 - 4)
StartsWet=1.0
StartStalls=0.0 //% of starts where driver stalled
QualifyingAbility=5.00 //Average qualifying position NOTE: keep GT between 1 -15
RaceAbility=1.2 //Range 0 - 6.2 (0 is best)
Consistency=1.0
RainAbility=1.5 //Range 0 - 6.2 (0 is best)
Passing=100.0 //% of times driver completed a successfull pass, not including pit stops or lapped traffic
Crash=4.0 //% of times driver crashed
Recovery=100 //% of times driver continued after a crash
CompletedLaps%=97
Script=default.scp
TrackAggression=1.0

// Increase attempted low-speed cornering by adding a minimum onto calculated speed.
// Reduce attempted high-speed cornering by multiplying speed by a number less than 1.0.
// <adjusted speed> = CorneringAdd + (CorneringMult * <original speed>)
CorneringAdd=1.0
CorneringMult=0.985

//AI Throttle Control - how good they are at their own traction control upon throttle application
TCGripThreshold=0.7 // Range: 0.0-1.0
TCThrottleFract=1.2 // Range: 0.0-???
TCResponse=0.0 // Range: 0.0-???

//AI skill mistake variables
MinRacingSkill = 1.00
Composure = 0.010

//AI ColdBrain variables
RaceColdBrainMin=0.99
RaceColdBrainTime=100
QualColdBrainMin=0.96
QualColdBrainTime=180

}
```



## Exclusive to Automobilista 1[^6]

### TireManagement

The lower the value, the more the AI will wear its tires.

Suggested range: 40-95 (80, 85 and 90 are the most common values)

### StartSkill

The lower the value, the more chance AI driver will bog down at the start.

Suggested range: 20-90, being around 70 the most commom value. If you set this under 60, the AI driver will problably bog down very often.




[^1]: Unless otherwise stated, sourced from [this Reiza Studios forum post](https://forum.reizastudios.com/threads/anyone-got-info-on-rcd-player-files.2941/) by Lorencini.
[^2]: From [jgf in this RaceDepartment post](https://www.racedepartment.com/threads/ai-editing-the-strength-of-a-mod-need-advice.228552/post-3520392).
[^3]: From [this post about rFactor 2 from the Studio 397 forums](https://forum.studio-397.com/index.php?threads/rcd-talent-file.52996/) (because of rF2's roots in isiMotor2).
[^4]: From [Bjarne Hansen on RaceDepartment](https://www.racedepartment.com/threads/how-to-find-the-right-ai-level-without-trying-them-all.160599/post-2853669).
[^5]: Provided by Jarrod Crossley [in this 2008 RaceDepartment post](https://www.racedepartment.com/threads/talent-files-and-the-ins-and-outs.604/#post-188618).
[^6]: From [this Reiza Studios forum post](https://forum.reizastudios.com/threads/anyone-got-info-on-rcd-player-files.2941/) by Lorencini.
